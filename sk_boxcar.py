import logging
import typing

import psr_formats
import numpy as np
import matplotlib.pyplot as plt

module_logger = logging.getLogger(__name__)


def get_nparts(
    ndat: int,
    nchan: int,
    time_window_size: int,
    time_window_overlap: int,
    freq_window_size: int,
    freq_window_overlap: int
) -> typing.Tuple[int]:
    time_window_diff = time_window_size - time_window_overlap
    freq_window_diff = freq_window_size - freq_window_overlap

    nparts_time = (ndat - time_window_diff) // time_window_overlap
    nparts_freq = (nchan - freq_window_diff) // freq_window_overlap
    if (nparts_freq != 1):
        nparts_freq += 1

    return nparts_time, nparts_freq


def compute_sk(
    arr: np.ndarray,
    time_window_size: int,
    time_window_overlap: int,
    freq_window_size: int,
    freq_window_overlap: int
) -> typing.Tuple[np.ndarray, np.ndarray]:
    """
    compute sum of power and sum of power squared. This will slide along in
    time and frequency.

    Args:
        arr (np.ndarray): (T, N) dimensional complex valued array, where N
            is number of channels, and T is number of time samples
        time_window_size (int): size of the accumulation window in the time
            dimension.
        time_window_overlap (int): size of the overlap region in the time
            dimension. Must be smaller than `time_window_size`
        freq_window_size (int): size of the accumulation window in the frequency
            dimension.
        freq_window_overlap (int): size of the overlap region in the frequency
            dimension. Must be smaller than `freq_window_size`

    Returns:
        tuple: two numpy arrays containing square of sums ("S1") and
            sum of squares ("S2")
    """
    ndat, nchan, npol = arr.shape
    module_logger.debug(f"compute_sk: ndat={ndat}, nchan={nchan}, npol={npol}")

    nparts_time, nparts_freq = get_nparts(
        ndat,
        nchan,
        time_window_size,
        time_window_overlap,
        freq_window_size,
        freq_window_overlap)

    module_logger.debug(
        f"compute_sk: nparts_time={nparts_time}, nparts_freq={nparts_freq}")

    # s1s = np.zeros((nparts_time, nparts_freq, npol))
    # s2s = np.zeros((nparts_time, nparts_freq, npol))
    estimates = np.zeros((nparts_time, nparts_freq, npol))

    M_factor = (time_window_size + 1) / (time_window_size - 1)  # going to leave this put

    for pdx in range(npol):
        for tdx in range(nparts_time):
            for fdx in range(nparts_freq):

                t_l = tdx*time_window_overlap
                t_u = t_l + time_window_size
                f_l = fdx*freq_window_overlap
                f_u = f_l + freq_window_size

                chunk = arr[t_l:t_u, f_l:f_u, pdx]

                chunk_power = chunk.real**2 + chunk.imag**2
                s1 = np.sum(chunk_power, axis=0)
                s2 = np.sum(chunk_power**2, axis=0)

                estimates_sub = M_factor * ((time_window_size*s2 / s1**2) - 1)

                estimates[tdx, fdx, pdx] = np.mean(estimates_sub)

                # s1s[tdx, fdx, pdx] = s1**2
                # s2s[tdx, fdx, pdx] = s2
    return estimates


def detect_sk(
    estimates: np.ndarray,
    sk_limits: typing.List[float]
) -> np.ndarray:

    masks = []
    for ipol in range(estimates.shape[-1]):
        mask = np.logical_or(
            estimates[:, :, ipol] > sk_limits[1],
            estimates[:, :, ipol] < sk_limits[0]
        )
        masks.append(mask)

    mask = np.logical_or(*masks)

    return mask


def mask_sk(
    arr: np.ndarray,
    mask: np.ndarray,
    time_window_size: int,
    time_window_overlap: int,
    freq_window_size: int,
    freq_window_overlap: int
):

    ndat, nchan, npol = arr.shape

    nparts_time, nparts_freq = get_nparts(
        ndat,
        nchan,
        time_window_size,
        time_window_overlap,
        freq_window_size,
        freq_window_overlap)

    for tdx in range(nparts_time):
        for fdx in range(nparts_freq):

            if mask[tdx, fdx]:
                t_l = tdx*time_window_overlap
                t_u = t_l + time_window_size
                f_l = fdx*freq_window_overlap
                f_u = f_l + freq_window_size
                arr[t_l:t_u, f_l:f_u, :] = 0.0

    return arr


def run_sk(
    arr: np.ndarray,
    time_window_size: int,
    time_window_overlap: int,
    freq_window_size: int,
    freq_window_overlap: int,
    std_dev: int = 3,
    sk_limits: typing.List[float] = None
):

    def compute_mu2(time_window_size):
        M = time_window_size
        return (4 * M * M) / ((M-1) * (M + 2) * (M + 3))

    def compute_sk_limits(freq_window_size, time_window_size):
        mu2 = compute_mu2(time_window_size)
        one_sigma_idat = np.sqrt(mu2 / float(freq_window_size))
        avg_upper_thresh = 1 + ((1+std_dev) * one_sigma_idat)
        avg_lower_thresh = 1 - ((1+std_dev) * one_sigma_idat)
        sk_limits = [
            avg_lower_thresh,
            avg_upper_thresh
        ]
        return sk_limits

    estimates = compute_sk(
        arr,
        time_window_size,
        time_window_overlap,
        freq_window_size,
        freq_window_overlap)

    if freq_window_size != 1:
        sk_limits = compute_sk_limits(freq_window_size, time_window_size)

    module_logger.debug(f"sk_limits={sk_limits}")

    mask = detect_sk(estimates, sk_limits)

    masked = mask_sk(
        arr.copy(),
        mask,
        time_window_size,
        time_window_overlap,
        freq_window_size,
        freq_window_overlap)

    return arr, masked


def plot_sk(
    arr: np.ndarray,
    masked: np.ndarray,
    time_window_size: int,
    time_window_overlap: int,
    freq_window_size: int,
    freq_window_overlap: int
):
    mean_zeros = np.mean(masked == 0)

    npol = arr.shape[-1]

    fig, axes = plt.subplots(2, npol, figsize=(10, 8))

    for ipol in range(npol):

        axes[0, ipol].imshow(np.abs(arr[:, :, ipol].T))
        axes[0, ipol].set_title(f"Original Signal, ipol={ipol}")

        axes[1, ipol].imshow(np.abs(masked[:, :, ipol].T))
        axes[1, ipol].set_title(
            (f"Masked Signal, ipol={ipol}: {mean_zeros*100:.2f} % masked\n"
             f"freq window size = {freq_window_size}, time window size = {time_window_size}\n"
             f"freq overlap size = {freq_window_overlap}, time overlap size = {time_window_overlap}"))

    for sub_list in axes:
        for ax in sub_list:
            ax.set_xlabel("Time")
            ax.set_ylabel("Channel")
            ax.set_aspect('auto')

    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    return fig, axes


def plot_sk_comparison(
    arr: np.ndarray,
    masked: np.ndarray,
    original_masked: np.ndarray,
    ipol: int = 0
) -> None:

    min_dat = min([masked.shape[0], original_masked.shape[0]])

    module_logger.debug(f"min_dat={min_dat}")

    arr = arr[:min_dat, :, :]
    masked = masked[:min_dat, :, :]
    original_masked = original_masked[:min_dat, :, :]

    mean_zeros = np.mean(masked == 0)
    mean_zeros_original = np.mean(original_masked == 0)

    fig, axes = plt.subplots(2, 2, figsize=(10, 5))

    axes[0, 0].imshow(np.abs(arr[:, :, ipol].T))
    axes[0, 0].set_title(f"Original Signal, ipol={ipol}")

    axes[1, 0].imshow(np.abs(masked[:, :, ipol].T))
    axes[1, 0].set_title(
        f"Masked Signal, ipol={ipol}: {mean_zeros*100:.2f} % masked")

    axes[0, 1].imshow(np.abs(original_masked[:, :, ipol].T))
    axes[0, 1].set_title(
        f"Original Masked Signal, ipol={ipol}: {mean_zeros_original*100:.2f} % masked")

    all_close_mean = np.mean(np.isclose(
        masked, original_masked))

    module_logger.debug(f"All close: {all_close_mean*100:.2f}%")

    for sub_list in axes:
        for ax in sub_list:
            if len(ax.images) != 0:
                ax.set_xlabel("Time")
                ax.set_ylabel("Channel")
                ax.set_aspect('auto')
            else:
                ax.axis("off")

    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    return fig, axes


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger("matplotlib").setLevel(logging.ERROR)
    file_paths = [
        "pre_SpectralKurtosis.dump",
        # "pre_Detection.skfb_only.dump"
        "pre_Detection.fscr_only.dump"
    ]

    comparison_params = {
        "pre_Detection.skfb_only.dump": {
            "time_window_size": 128,
            "time_window_overlap": 128,
            "freq_window_size": 1,
            "freq_window_overlap": 1,
            "sk_limits": [0.601904, 1.76297],
            "std_dev": 3,
            "title": "SKFB Comparison",
            "file_name": "skfb_comparison.png"
        },
        "pre_Detection.fscr_only.dump": {
            "time_window_size": 128,
            "time_window_overlap": 128,
            "freq_window_size": 4096,
            "freq_window_overlap": 1,
            "sk_limits": [0.98912, 1.01088],
            "std_dev": 3,
            "title": "fscr Comparison",
            "file_name": "fscr_comparison.png"
        }
    }



    dada_file0 = psr_formats.DADAFile(file_paths[0]).load_data()

    if len(file_paths) == 1:

        time_window_size = 512
        time_window_overlap = 64
        freq_window_size = 64
        freq_window_overlap = 8
        std_dev = 3
        sk_limits = [0.601904, 1.76297]  # from DSPSR logs, for skfb only

        arr, masked = run_sk(
            dada_file0.data.copy(),
            time_window_size,
            time_window_overlap,
            freq_window_size,
            freq_window_overlap,
            std_dev,
            sk_limits
        )

        fig, ax = plot_sk(
            arr,
            masked,
            time_window_size,
            time_window_overlap,
            freq_window_size,
            freq_window_overlap)

        fig.suptitle("SK boxcar")
        fig.savefig(f"sk_boxcar.{time_window_size}.{time_window_overlap}.{freq_window_size}.{freq_window_overlap}.png")

    else:
        params = comparison_params[file_paths[1]]
        dada_file1 = psr_formats.DADAFile(file_paths[1]).load_data()
        arr, masked = run_sk(
            dada_file0.data.copy(),
            params["time_window_size"],
            params["time_window_overlap"],
            params["freq_window_size"],
            params["freq_window_overlap"],
            params["std_dev"],
            params["sk_limits"]
        )
        fig, axes = plot_sk_comparison(arr, masked, dada_file1.data)

        fig.suptitle(params["title"])
        fig.savefig(params["file_name"])

        plt.show()
